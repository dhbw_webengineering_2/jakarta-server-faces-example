plugins {
    `java-library`
    `maven-publish`
    alias(libs.plugins.com.github.ben.manes.versions)
    alias(libs.plugins.nl.littlerobots.version.catalog.update)
}

repositories {
    mavenLocal()
    mavenCentral()
    maven ( url = "https://jitpack.io" )
}

dependencies {
    implementation(libs.org.apache.tomcat.embed.tomcat.embed.core)
    implementation(libs.org.apache.tomcat.embed.tomcat.embed.websocket)
    implementation(libs.org.apache.tomcat.embed.tomcat.embed.jasper)
    implementation(libs.org.apache.tomcat.tomcat.jasper)
    implementation(libs.org.apache.tomcat.tomcat.jasper.el)
    implementation(libs.org.apache.tomcat.tomcat.jsp.api)
    implementation(libs.jakarta.faces.jakarta.faces.api)
    implementation(libs.org.glassfish.jakarta.faces)
    implementation(libs.jakarta.websocket.jakarta.websocket.api)
    implementation(libs.org.jboss.weld.servlet.weld.servlet.shaded)
    implementation(libs.jakarta.servlet.jsp.jstl.jakarta.servlet.jsp.jstl.api)
//    implementation("jakarta.servlet:jakarta.servlet-api:5.0.0")
//    implementation("com.sun.faces:jsf-impl:2.2.0")
}

group = "org.dhbw.karlsruhe.webengineering"
version = "1.0-SNAPSHOT"
description = "jsf"
java.sourceCompatibility = JavaVersion.VERSION_17

publishing {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
}
